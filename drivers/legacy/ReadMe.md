# Legacy Driver

Original driver to execute the initial original run format.
This includes a binary geometry format and two configuration files.

NOT IN FUNCTIONAL CONDITION

This original format has been expanded upon and the legacy format is no longer supported.
If we do need to roll back support for the original format, this will need to be updated.

We have migrated to an open source VTK format for geometry and created a single configuration file to support fluid, geometry, and simulation parameters.