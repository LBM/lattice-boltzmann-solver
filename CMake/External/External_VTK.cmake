#-----------------------------------------------------------------------------
# Add External Project
#-----------------------------------------------------------------------------

set(VTK_MODULE_SETTINGS
    -DModule_vtkIOXML:BOOL=ON
    -DModule_vtkIOLegacy:BOOL=ON
    -DModule_vtkIOGeometry:BOOL=ON
    -DModule_vtkFiltersCore:BOOL=ON
    -DModule_vtkFiltersGeometry:BOOL=ON
    -DModule_vtkChartsCore:BOOL=ON
    -DModule_vtkViewsContext2D:BOOL=ON
  )
if(LBM_VTK_OFFSCREEN)
  list(APPEND VTK_MODULE_SETTINGS
    -DVTK_OPENGL_HAS_OSMESA:BOOL=ON
    -DVTK_DEFAULT_RENDER_WINDOW_OFFSCREEN:BOOL=ON
    -DVTK_USE_X:BOOL=OFF
  )
endif()

include(AddExternalProject)
add_external_project_ex( VTK
  URL https://gitlab.kitware.com/vtk/vtk/-/archive/v8.2.0/vtk-v8.2.0.tar.gz
  URL_MD5 4115fb396f99466fe444472f412118cd
  CMAKE_CACHE_ARGS
       ${VTK_MODULE_SETTINGS}
      -DBUILD_EXAMPLES:BOOL=OFF
      -DBUILD_TESTING:BOOL=OFF
      -DVTK_Group_StandAlone:BOOL=OFF
      -DVTK_Group_Rendering:BOOL=OFF
      -DVTK_WRAP_PYTHON:BOOL=OFF
      -DVTK_LEGACY_REMOVE:BOOL=ON
      -DVTK_BUILD_TESTING:STRING=OFF
  DEPENDENCIES ${VTK_DEPENDENCIES}
  RELATIVE_INCLUDE_PATH ""
)
if(NOT USE_SYSTEM_VTK)
  set(VTK_DIR ${CMAKE_INSTALL_PREFIX}/cmake/vtk-8.2)
  #message(STATUS "VTK_DIR : ${VTK_DIR}")
endif()
