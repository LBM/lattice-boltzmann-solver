#-----------------------------------------------------------------------------
# Add External Project
#-----------------------------------------------------------------------------
include(AddExternalProject)
add_external_project_ex( ITK
  #URL https://github.com/InsightSoftwareConsortium/ITK/archive/v5.0.1.tar.gz
  #URL_HASH MD5=15f0422aab814309e9d95c55cbf09c22
  URL https://github.com/InsightSoftwareConsortium/ITK/archive/v5.1rc01.tar.gz
  URL_HASH MD5=5dc33b74e4400ca468cbc8f14dd99de4
  CMAKE_CACHE_ARGS
    -DINSTALL_GTEST:BOOL=OFF
    -DBUILD_DOCUMENTATION:BOOL=OFF
    -DBUILD_EXAMPLES:BOOL=OFF
    -DBUILD_SHARED_LIBS:BOOL=OFF
    -DBUILD_TESTING:BOOL=OFF
    -DITK_LEGACY_REMOVE:BOOL=ON
    -DITK_USE_SYSTEM_EIGEN:BOOL=ON
    -DEigen3_DIR:PATH=${Eigen3_DIR}
  RELATIVE_INCLUDE_PATH "include"
  DEPENDENCIES
    Eigen3
  #VERBOSE
)

if(NOT USE_SYSTEM_ITK)
  set(ITK_DIR ${CMAKE_INSTALL_PREFIX}/cmake/ITK-5.1)
  #message(STATUS "ITK_DIR : ${ITK_DIR}")
endif()
